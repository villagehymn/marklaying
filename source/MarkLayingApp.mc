using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.Math as Math;
using Toybox.System as Sys;

enum {

	VIEW_COURSE,
	VIEW_BEARING,
	VIEW_TIME
}

enum {

	TYPE_TRAP60,
	TYPE_TRAP70,
	TYPE_TRAP80,
	TYPE_TRAP90,
	TYPE_TRI60,
	TYPE_TRI45
}

class MarkLayingApp extends App.AppBase {

	var _view;
	var _bearing;
	var _bearingleft;
	var _bearingright;
	var _reverse;
	var _course;
	var _coursetype;
	var _markfrom;
	var _markto;
	var _marksmax;
	var _markbearing;
	var _markreverse;
	var _markshow;
	var _markdistance;
	var _lengthbeat;
	var _lengthreach;
	
	var _mlview;
	var _mldelegate;
	
	var _timervalue;
	var _timerstarted;
	

    function initialize() {
    
        AppBase.initialize();
        
        _view = VIEW_BEARING;
        _bearing = 0;
        _bearingleft = 270;
        _bearingright = 90;
        _reverse = 180;
        _course = 0;
        _coursetype = TYPE_TRAP70;

		_timerstarted = false;
        
        _markfrom = 1;
        _markto = 2;
        _marksmax = 5;
        _markbearing = 0;
        _markreverse = 180;
        _markshow = false;
        _markdistance = 0;
        
        _lengthbeat = 1.0;
        _lengthreach = 0.666; 
        
        _mlview = new MarkLayingView();
        _mldelegate = new MarkLayingDelegate();
        
        calcMarkAngles();
    }
    
    function getLengthBeat() {
    
    	return _lengthbeat;
    }
    
    function getLengthReach() {
    	
    	return _lengthreach;
    }
    
    function setLengthBeat( val ) {
    
    	_lengthbeat = val;
    	calcMarkAngles();
    }
    
    function setLengthReach( val ) {
    
    	_lengthreach = val;
    	calcMarkAngles();
    }
    
    function getCourseTypeString() {
    
    	switch( _coursetype ) {
    	
		case TYPE_TRAP60:
			return "trap 60";
		case TYPE_TRAP70:
			return "trap 70";
		case TYPE_TRAP80:
			return "trap 80";
		case TYPE_TRAP90:
			return "trap 90";
		case TYPE_TRI60:
			return "tri 60";
		case TYPE_TRI45:
			return "tri 45";
    	}
    
    	return "unknown";
    }
    
    function getMarkDistance() {
    
    	return _markdistance;
    }
    
    function getMarkShow() {
    
    	return _markshow;
    }
    
    function getMarkFrom() {
    
    	return _markfrom;
    }
    
    function getMarkTo() {
    
    	return _markto;
    }    
    
    function increaseMarkFrom() {
    
    	_markfrom = _markfrom + 1;
    	
    	if(_markfrom == _markto) {
    	
    		_markfrom = _markfrom + 1;
    	}
    		
    	if(_markfrom > _marksmax) {
    	
    		_markfrom = 1;
    	}
    	
    	calcMarkAngles();
    }
    
    function decreaseMarkFrom() {
    
    	_markfrom = _markfrom - 1;

    	if(_markfrom < 1) {
    	
    		_markfrom = _marksmax;
    	}    
    	
    	if(_markfrom == _markto) {
    	
    		_markfrom = _markfrom - 1;
    	}
    	
    	calcMarkAngles();
    }    
    
    function increaseMarkTo() {
    
    	_markto = _markto + 1;
    	
    	if(_markfrom == _markto) {
    	
    		_markto = _markto + 1;
    	}
    		
    	if(_markto > _marksmax) {
    	    	
    		_markto = 1;
    	}    
    	
    	calcMarkAngles();
    }
    
    function decreaseMarkTo() {
    
    	_markto = _markto - 1;

    	if(_markto < 1) {
    	
    		_markto = _marksmax;
    	}    
    	
    	if(_markfrom == _markto) {
    	
    		_markto = _markto - 1;
    	}    
    	
    	calcMarkAngles();
    }    
    
    function setCourseType( type ) {
    
    	_coursetype = type;
    	
    	if(( _coursetype == TYPE_TRAP60 ) || (_coursetype == TYPE_TRAP70) || (_coursetype == TYPE_TRAP80) || (_coursetype == TYPE_TRAP90)) { 
    	
    		_marksmax = 5;	
    	} else {
    	
    		_marksmax = 3;
    	}
    	
    	calcMarkAngles();
    }

	function getMLView() {
	
		return _mlview;
	} 
	
	function getMLDelegate() {
	
		return _mldelegate;
	}

	function setViewTime() {
		
		_view = VIEW_TIME;
		_mlview.setView(_view);
	}

	function setViewCourse() {
		
		_view = VIEW_COURSE;
		_mlview.setView(_view);
	}
	
	function setViewBearing() {
	
		_view = VIEW_BEARING;
		_mlview.setView(_view);
	}
		
	function getView() {
	
		return _view;
	}
	
	function getBearing() { 
	
		return _bearing;
	}
	
	function getBearingLeft() { 
	
		return _bearingleft;
	}
	
	function getBearingRight() { 
	
		return _bearingright;
	}
	
	function getReverse() { 
	
		return _reverse;
	}	
	
	function getMarkBearing() { 
	
		return _markbearing;
	}
	
	function getMarkReverse() { 
	
		return _markreverse;
	}	
		
	
	function getCourse() { 
	
		return _course;
	}		
	
	function calcReverse( bearing ) {
	
		if(bearing < 180) {
		
			return (180 + bearing);
		
		} else {
		
			return (bearing - 180);
		}
	}
	
	function increaseAngle( value, amount ) {

		value = value + amount;
		
		if (value > 359) {
			value = value - 360;
		}	
		
		return value;
	}
	
	function decreaseAngle( value, amount ) {
	
		value = value - amount;
		
		if(value < 0) {
			value = value + 360;
		}
			
		return value;
	}

	function updateBearing() {

		_bearingright = increaseAngle(_bearing,90);
		_bearingleft = decreaseAngle(_bearing,90);		
		_reverse = calcReverse(_bearing);	
	}

	function courseUp() {

		_course = increaseAngle(_course,10);
		_bearing = _course;
		updateBearing();
		calcMarkAngles();	
	}
	
	function courseDown() {

		_course = decreaseAngle(_course,1);
		_bearing = _course;
		updateBearing();
		calcMarkAngles();	
	}
	
	function getIsTimerStarted() {
	
		return _timerstarted;
	}
	
	function getTimerStart() {
	
		return _timervalue;
	}

	function doSelect() {
	
		if(_view == VIEW_TIME) {
		
			//start the timer if it's not started
			if(_timerstarted == false) {

				_timerstarted = true;
				_timervalue = Time.now();
			} else {
			
				_timerstarted = false;
			}			
		}
	}

	function goUp() {
	
		if(_view == VIEW_BEARING) {
		
			_bearing = increaseAngle(_bearing,10);
			updateBearing();

		} else if (_view == VIEW_COURSE) {
		
			increaseMarkFrom();
		} else {
		
		}
	}
	
	function goDown() {
	
		if(_view == VIEW_BEARING) {
		
			_bearing = decreaseAngle(_bearing,1);
			updateBearing();
			
		} else if (_view == VIEW_COURSE) {
		
			increaseMarkTo();
		} else {
		
			if(_timerstarted) {
			
				var currval = Time.now().compare(_timervalue); 
			
				if(currval < (5 * 60)) {
			
					currval = -(60 - (currval % 60));
					_timervalue = _timervalue.add(new Time.Duration(currval));
				}
			}
		}
	}
	
	function calcMarkAngles() {
	
		_markshow = false;
	
		//_markbearing
		if(( _coursetype == TYPE_TRAP60 ) || (_coursetype == TYPE_TRAP70) || (_coursetype == TYPE_TRAP80) || (_coursetype == TYPE_TRAP90)) {

			var innerangle = 60;

			if(_coursetype == TYPE_TRAP70) {
			
				innerangle = 70;
			}if(_coursetype == TYPE_TRAP80) {
			
				innerangle = 80;
			}if(_coursetype == TYPE_TRAP90) {
			
				innerangle = 90;
			}
		
			if(((_markfrom == 4) && (_markto == 1)) || ((_markfrom == 3) && (_markto == 2))) {
			
				_markbearing = _course;
				_markshow = true;
				_markdistance = _lengthbeat;
				
			} else if(((_markfrom == 1) && (_markto == 4)) || ((_markfrom == 2) && (_markto == 3))) {
			
				_markbearing = calcReverse(_course);
				_markshow = true;
				_markdistance = _lengthbeat;
				
			} else if(((_markfrom == 1) && (_markto == 2)) || ((_markfrom == 4) && (_markto == 3))) {
			
				innerangle = 180 + innerangle;
						
				_markbearing = increaseAngle(_course,innerangle);
				_markshow = true;
				_markdistance = _lengthreach;
				
			} else if(((_markfrom == 2) && (_markto == 1)) || ((_markfrom == 3) && (_markto == 4))) {
						
				_markbearing = increaseAngle(_course,innerangle);
				_markshow = true;
				_markdistance = _lengthreach;
				
			} else if((_markfrom == 3) && (_markto == 5)) {
			
				_markbearing = increaseAngle(_course,180 - innerangle);
				_markshow = true;
				_markdistance = 0.15;
				
			} else if((_markfrom == 5) && (_markto == 3)) {
			
				_markbearing = decreaseAngle(_course,innerangle);
				_markshow = true;
				_markdistance = 0.15;
				
			} else if(((_markfrom == 4) && (_markto == 2)) || ((_markfrom == 2) && (_markto == 4))) {
			
				var innerradians = Math.toRadians(innerangle);
				
				_markdistance = (_lengthbeat * _lengthbeat) + (_lengthreach * _lengthreach) - (2 * _lengthbeat * _lengthreach * Math.cos(innerradians));
				_markdistance = Math.sqrt(_markdistance);
				
				_markbearing = (_lengthreach * Math.sin(innerradians)) / _markdistance;
				_markbearing = Math.toDegrees(Math.asin(_markbearing));
								
				
				if(_markfrom == 2) {
				
					_markbearing = calcReverse(_markbearing);
				}
				
				_markbearing = decreaseAngle(_course,_markbearing);
				
				_markshow = true;
			}					
			
		} else {
		
			
		}
		
		_markreverse = Math.round(calcReverse(_markbearing));
		_markbearing = Math.round(_markbearing);
	}

    // onStart() is called on application start up
    function onStart(state) {
    }

    // onStop() is called when your application is exiting
    function onStop(state) {
    }

    // Return the initial view of your application here
    function getInitialView() {
    
        return [ _mlview, _mldelegate ];
    }

}

