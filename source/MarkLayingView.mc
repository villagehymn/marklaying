using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Timer as Timer;
using Toybox.Application as App;
using Toybox.System;
using Toybox.Time as Time;
using Toybox.Time.Gregorian;

class MarkLayingView extends Ui.View {

	var _screenHeight;
	var _screenWidth;
	var _uiTimer;
	var _view;

    function initialize() {
        View.initialize();
        _view = VIEW_BEARING;
    }

	function setView( view ) {
	
		_view = view;
	}

    // Load your resources here
    function onLayout(dc) {
    
        _screenWidth = dc.getWidth();
        _screenHeight = dc.getHeight();    
    
        setLayout(Rez.Layouts.MainLayout(dc));        
        
        _uiTimer = new Timer.Timer();
        _uiTimer.start(method(:refreshUi), 100, true);
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
    
    	var app = App.getApp();
    	var view = _view; 
    
	    dc.setColor( Gfx.COLOR_TRANSPARENT, Gfx.COLOR_BLACK );
		dc.clear();
		dc.setColor( Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT );
    
    	if(view == VIEW_BEARING) {
	        
	        //dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - (Gfx.getFontHeight(Gfx.FONT_NUMBER_THAI_HOT) / 2), Gfx.FONT_NUMBER_THAI_HOT, "01:23", Gfx.TEXT_JUSTIFY_CENTER );	        
	        //dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - (Gfx.getFontHeight(Gfx.FONT_NUMBER_THAI_HOT) / 2), Gfx.FONT_NUMBER_THAI_HOT, "01:23", Gfx.TEXT_JUSTIFY_CENTER );
	        
	        dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - (2 * Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, "bearing: " + app.getBearing(), Gfx.TEXT_JUSTIFY_CENTER );
	        dc.drawText( (_screenWidth / 2), (_screenHeight / 2) 										  , Gfx.FONT_LARGE, "l: " + app.getBearingLeft() + " r: " +  app.getBearingRight(), Gfx.TEXT_JUSTIFY_CENTER );
	        dc.drawText( (_screenWidth / 2), (_screenHeight / 2) + (Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, "reverse: " + app.getReverse(), Gfx.TEXT_JUSTIFY_CENTER );
        } else if (view == VIEW_COURSE ) {
        
        	dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - 20 - (2 * Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_SMALL, app.getCourseTypeString() + " / axis: " + app.getCourse(), Gfx.TEXT_JUSTIFY_CENTER );
        	dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - 20 - (Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, "m" + app.getMarkFrom() + " to m" + app.getMarkTo(), Gfx.TEXT_JUSTIFY_CENTER );
        	
        	if(app.getMarkShow()) {
        	
		        dc.drawText( (_screenWidth / 2), (_screenHeight / 2)  - 20									  , Gfx.FONT_LARGE, "bearing: " + app.getMarkBearing().toNumber(), Gfx.TEXT_JUSTIFY_CENTER );
		        dc.drawText( (_screenWidth / 2), (_screenHeight / 2)  - 20 + (Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, "reverse: " + app.getMarkReverse().toNumber(), Gfx.TEXT_JUSTIFY_CENTER );
		        dc.drawText( (_screenWidth / 2), (_screenHeight / 2)  - 20 + (2 * Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, "dist: " + app.getMarkDistance().format("%4.3f"), Gfx.TEXT_JUSTIFY_CENTER );
		        
		    }
        } else {
        
			var today = Gregorian.info(Time.now(), Time.FORMAT_SHORT);
			var dateString = Lang.format("$1$:$2$:$3$",[today.hour,today.min,today.sec]);        
        
        	var seconds = -5 * 60;
    
    		if(app.getIsTimerStarted()) {
        	
        		seconds = seconds + Time.now().compare(app.getTimerStart());
        	}
        
        	if(seconds < 0) {

				var moment = new Time.Moment(-seconds);
				var present = Gregorian.info(moment, Time.FORMAT_MEDIUM);

        		dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - 20 - (2 * Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, "countdown", Gfx.TEXT_JUSTIFY_CENTER );
        		dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - 20 - (1 * Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, Lang.format("$1$:$2$",[present.min,present.sec]), Gfx.TEXT_JUSTIFY_CENTER );        	
        	} else {
        	    
        	    var moment = new Time.Moment(seconds);
        	    var present = Gregorian.info(moment, Time.FORMAT_MEDIUM);
        	        
        		dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - 20 - (2 * Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, "timer", Gfx.TEXT_JUSTIFY_CENTER );
        		dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - 20 - (1 * Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, Lang.format("$1$:$2$",[present.min,present.sec]), Gfx.TEXT_JUSTIFY_CENTER );
        	}
        	
        	dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - 20 + (1 * Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, "clock", Gfx.TEXT_JUSTIFY_CENTER );
        	dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - 20 + (2 * Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, dateString, Gfx.TEXT_JUSTIFY_CENTER );
        }
        
        
        //View.onUpdate(dc);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }
    
    function refreshUi(){
    	Ui.requestUpdate();
    }
        
}

