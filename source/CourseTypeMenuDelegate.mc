using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Application as App;

class CourseTypeMenuDelegate extends Ui.MenuInputDelegate {

    function initialize() {
        MenuInputDelegate.initialize();
    }

    function onMenuItem(item) {
    
    	var app = App.getApp();
    
        if (item == :coursetypeitem_1) {
            
            app.setCourseType(TYPE_TRAP60);
            
        } else if (item == :coursetypeitem_2) {
        
        	app.setCourseType(TYPE_TRAP70);
            
		} else if (item == :coursetypeitem_3) {
          
           	app.setCourseType(TYPE_TRAP80);
            
		} else if (item == :coursetypeitem_4) {
        
        	app.setCourseType(TYPE_TRAP90);
            
		} else if (item == :coursetypeitem_5) {
		
			app.setCourseType(TYPE_TRI60);
			
		} else if (item == :coursetypeitem_6) {
		
			app.setCourseType(TYPE_TRI45);
		}
		
		//Ui.popView(Ui.SLIDE_IMMEDIATE);
    }
}