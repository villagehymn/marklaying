using Toybox.WatchUi as Ui;
using Toybox.Application as App;

class SetCourseDelegate extends Ui.BehaviorDelegate {

    function initialize() {
        BehaviorDelegate.initialize();
    }
   
   	function onSelect() {
   	
   		Ui.popView(Ui.SLIDE_IMMEDIATE);
   	}
   
    function onPreviousPage() {
    
    	App.getApp().courseUp();
    }
    
    function onNextPage() {
    
    	App.getApp().courseDown();
    }

}