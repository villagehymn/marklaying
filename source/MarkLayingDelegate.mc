using Toybox.WatchUi as Ui;
using Toybox.Application as App;

class MarkLayingDelegate extends Ui.BehaviorDelegate {

    function initialize() {
        BehaviorDelegate.initialize();
    }

    function onMenu() {
    
        Ui.pushView(new Rez.Menus.MainMenu(), new MarkLayingMenuDelegate(), Ui.SLIDE_IMMEDIATE);
    }
    
    function onPreviousPage() {
    
    	App.getApp().goUp();
    }
    
    function onNextPage() {
    
    	App.getApp().goDown();
    }
    
    function onSelect() {

    	App.getApp().doSelect(); 
    }
}