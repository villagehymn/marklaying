using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Application as App;

class MarkLayingMenuDelegate extends Ui.MenuInputDelegate {

    function initialize() {
        MenuInputDelegate.initialize();
    }

    function onMenuItem(item) {
        if (item == :item_1) {
            
            App.getApp().setViewCourse();
            
        } else if (item == :item_2) {
        
        	App.getApp().setViewBearing();
            
		} else if (item == :item_3) {
		
			App.getApp().setViewTime();
			
		} else if (item == :item_4) {
		
			Ui.pushView(new Rez.Menus.SetupMenu(), new SetupMenuDelegate(), Ui.SLIDE_IMMEDIATE);            
        }
    }

}