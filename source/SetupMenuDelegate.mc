using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Application as App;
using Toybox.Graphics as Gfx;

class BeatPickerDelegate extends Ui.NumberPickerDelegate {
    function initialize() {
        NumberPickerDelegate.initialize();
    }

    function onCancel() {
        Ui.popView(Ui.SLIDE_IMMEDIATE);
    }

    function onAccept(values) {
        App.getApp().setLengthBeat(values[0]); // e.g. 1000f
        Ui.popView(Ui.SLIDE_IMMEDIATE);
    }
}

class ReachPickerDelegate extends Ui.NumberPickerDelegate {
    function initialize() {
        NumberPickerDelegate.initialize();
    }

    function onCancel() {
        Ui.popView(Ui.SLIDE_IMMEDIATE);
    }

    function onAccept(values) {
        App.getApp().setLengthReach(values[0]); // e.g. 1000f
        Ui.popView(Ui.SLIDE_IMMEDIATE);
    }
}

class SetupMenuDelegate extends Ui.MenuInputDelegate {

    function initialize() {
        MenuInputDelegate.initialize();
    }

    function onMenuItem(item) {
    
        if (item == :setupitem_1) {
        
        	Ui.pushView(new Rez.Menus.CourseTypeMenu(), new CourseTypeMenuDelegate(), Ui.SLIDE_IMMEDIATE);
            
		} else if (item == :setupitem_2) {
		
			//Set course axis        	       	        	
        	Ui.pushView(new SetCourseView(), new SetCourseDelegate(), Ui.SLIDE_IMMEDIATE);
        	
		} else if (item == :setupitem_3) {
		
			var size = App.getApp().getLengthBeat();
			
	        var title = new Ui.Text({:text=>"Beat", :locX =>Ui.LAYOUT_HALIGN_CENTER, :locY=>Ui.LAYOUT_VALIGN_BOTTOM, :color=>Gfx.COLOR_WHITE});
	        var factory = new NumberFactory(0.0,2.0,0.05,null);
	        var index = factory.getIndex(size);
	        
           	var myPicker = new Ui.Picker({:title=>title, :pattern=>[factory], :defaults=>[index]});
            
            WatchUi.pushView(
                myPicker,
                new BeatPickerDelegate(),
                WatchUi.SLIDE_IMMEDIATE
            );		
		
		
		} else if (item == :setupitem_4) {

			var size = App.getApp().getLengthReach();
			
	        var title = new Ui.Text({:text=>"Reach", :locX =>Ui.LAYOUT_HALIGN_CENTER, :locY=>Ui.LAYOUT_VALIGN_BOTTOM, :color=>Gfx.COLOR_WHITE});
	        var factory = new NumberFactory(0.0,2.0,0.05,null);
	        var index = factory.getIndex(size);
	        
           	var myPicker = new Ui.Picker({:title=>title, :pattern=>[factory], :defaults=>[index]});
            
            WatchUi.pushView(
                myPicker,
                new ReachPickerDelegate(),
                WatchUi.SLIDE_IMMEDIATE
            );					            
        }
    }
}