using Toybox.WatchUi as Ui;
using Toybox.Application as App;
using Toybox.Graphics as Gfx;

class SetCourseView extends Ui.View {

    function initialize() {
        View.initialize();
    }	

    function onUpdate(dc) {
    
    	var app = App.getApp();
        var _screenWidth = dc.getWidth();
        var _screenHeight = dc.getHeight();      	
    
	    dc.setColor( Gfx.COLOR_TRANSPARENT, Gfx.COLOR_BLACK );
		dc.clear();
		dc.setColor( Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT );
            
        dc.drawText( (_screenWidth / 2), (_screenHeight / 2) - (Gfx.getFontHeight(Gfx.FONT_LARGE)), Gfx.FONT_LARGE, "course: " + app.getCourse(), Gfx.TEXT_JUSTIFY_CENTER );    
    }
}
